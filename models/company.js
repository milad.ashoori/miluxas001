const mongoose = require('mongoose')
const mongoosastic = require('mongoosastic')
const Joi = require('@hapi/joi')
Joi.objectId = require('joi-objectid')(Joi);

const companySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    logo: {
        type: String,
        required: true,
    },
    created_by:{type: mongoose.Schema.Types.ObjectId},
    users:[
        {type: mongoose.Schema.Types.ObjectId}
    ],
    updated_time: Date,
    created_time: Date,
})

companySchema.plugin(mongoosastic, {
    host: 'localhost',
    port: 9200,
})

function validate(company) {
    const schema = Joi.object({
        name: Joi.string(),
        logo: Joi.string(),
    })
    return schema.validate(company)
}

function validateOnCreate(company) {
    const schema = Joi.object({
        name: Joi.string().required(),
        logo: Joi.string().required(),
    })
    return schema.validate(company)
}

const Company = mongoose.model('company', companySchema)
Company.validate = validate
Company.validateOnCreate = validateOnCreate

exports.Company = Company
