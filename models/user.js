const mongoose = require('mongoose')
const mongoosastic = require('mongoosastic')
const Joi = require('@hapi/joi')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    updated_time: Date,
    created_time: Date,
})

userSchema.plugin(mongoosastic, {
    host: 'localhost',
    port: 9200,
})

function validate(user) {
    const schema = Joi.object({
        name: Joi.string(),
        password: Joi.string(),
    })
    return schema.validate(user)
}

function validateOnCreate(user) {
    const schema = Joi.object({
        name: Joi.string().required(),
        password: Joi.string().required(),
    })
    return schema.validate(user)
}

const User = mongoose.model('User', userSchema)
User.validate = validate
User.validateOnCreate = validateOnCreate

exports.User = User
