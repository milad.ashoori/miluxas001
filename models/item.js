const mongoose = require('mongoose')
const mongoosastic = require('mongoosastic')
const Joi = require('@hapi/joi')
Joi.objectId = require('joi-objectid')(Joi);

const itemSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    logo: {
        type: String,
        required: true,
    },

    created_by:{
        type: mongoose.Schema.Types.ObjectId,
    },

    company:{
        type: mongoose.Schema.Types.ObjectId,
    },
    updated_time: Date,
    created_time: Date,
})

itemSchema.plugin(mongoosastic, {
    host: 'localhost',
    port: 9200,
})

function validate(item) {
    const schema = Joi.object({
        name: Joi.string(),
        logo: Joi.string(),
    })
    return schema.validate(item)
}

function validateOnCreate(item) {
    const schema = Joi.object({
        name: Joi.string().required(),
        logo: Joi.string().required(),
    })
    return schema.validate(item)
}

const Item = mongoose.model('item', itemSchema)
Item.validate = validate
Item.validateOnCreate = validateOnCreate

exports.Item = Item
