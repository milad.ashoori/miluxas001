const RouterManager = require('./routes/route_manager')
const { Company } = require('./models/company')
const { User } = require('./models/user')
const { Item } = require('./models/item')

module.exports = function (app) {
    app.use('/api/companies',new RouterManager(Company))
    app.use('/api/users',new RouterManager(User))
    app.use('/api/items',new RouterManager(Item))
}
