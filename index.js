const express = require('express') // call express
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const options = {
    autoIndex: true, // Don't build indexes
    //  reconnectTries: 30, // Retry up to 30 times
    //  reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    useNewUrlParser: true,
    useUnifiedTopology: true,
}


const app = express() // define our app using express
app.use(express.json())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
require('./routes')(app)

mongoose
        .connect(
            'mongodb://127.0.0.1:27017/myapp',
            options,
        )
        .then(() => {
            console.log('MongoDB is connected')
        })
        .catch((err) => {
            console.log('MongoDB connection unsuccessful, retry after 5 seconds.', err)
            setTimeout(connectWithRetry, 5000)
        })

    mongoose.Promise = global.Promise
    mongoose.set('useNewUrlParser', true)
    mongoose.set('useFindAndModify', false)
    mongoose.set('useCreateIndex', true)





const port =3030

const server = app.listen(port, () => {})
console.log('SERVER IS UP AND RUNNING ...')

module.exports = server
