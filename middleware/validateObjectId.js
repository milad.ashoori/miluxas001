const mongoose = require('mongoose')

module.exports = function (req, res, next) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
        return res.status(400).send({
            error: true,
            result_number: 140002,
            message: 'Input data not correct: Invalid ID',
        })
    }

    next()
}
