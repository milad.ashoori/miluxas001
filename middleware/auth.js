const jwt = require('jsonwebtoken')

module.exports = function (req, res, next) {
    const token = req.headers.authorization
    if (!token) {
        return res.status(401).send({
            error: true,
            result_number: 140101,
            message: 'Authorization not found',
        })
    }

    try {
        const decoded = jwt.verify(token.trim().slice(4), "123456789")
        req.user = decoded
        if (req.headers.organization) {
            req.related_organization = req.headers.organization
        }
        next()
    } catch (ex) {
        res.status(401).send({
            error: true,
            result_number: 140102,
            message: 'Invalid authorization',
        })
    }
}
