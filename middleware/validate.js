module.exports = (validator) => {
    return (req, res, next) => {
        const { error } = validator(req.body)
        if (error) {
            return res.status(400).send({
                error: true,
                result_number: 140002,
                message: 'Input data not correct: ' + param_error.details[0].message,
            })
        }
        next()
    }
}
