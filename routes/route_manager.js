const _ = require('lodash')
const auth = require('../middleware/auth')
const express = require('express')
const { Model } = require('mongoose');

class RouterManager extends express.Router{
    /**
     * 
     * @param {Model} MoBaModel 
     */
    constructor(MoBaModel){
        super();

        var props=Object.keys(MoBaModel.schema.obj);
        var changable_props=props.filter(pr=>pr!="updated_time" && pr!="created_time" && pr!="company" && pr!="created_by");
        var refsList=Array();
        props.map(p=>{
            if(MoBaModel.schema.obj[p].ref)
                refsList.push(p);
        })
        
        var isCompanyPrivateData = props.find(pr=>
            pr==='company'
        )!==undefined;

        console.log(isCompanyPrivateData);

        var mlPopulate= (fun,refsList)=>{
            if(refsList===undefined || refsList.length===0)
                return fun;
            
            var refName=refsList.pop();
            return mlPopulate(fun.populate(refName));
        }

        this.get('/', async (req, res) => {
            var moBaModel =[];
            if(isCompanyPrivateData){
                moBaModel = await mlPopulate(MoBaModel.find({ 'company': '5f007ef891e0d33f080b9972' }),refsList.slice()).lean().exec()
            }
            else
            {
                moBaModel = await mlPopulate(MoBaModel.find(),refsList.slice()).lean().exec()
            }
        
            res.send({
                error: false,
                result_number: moBaModel.length,
                message: moBaModel,
            })
        });
        
        this.get('/:id', async (req, res) => {

            var moBaModel =[];
            if(isCompanyPrivateData){
                moBaModel = await mlPopulate(MoBaModel.findOne({ _id: req.params.id , 'company': '5f007ef891e0d33f080b9972' }),refsList.slice()).lean().exec()
            }
            else
            {
                moBaModel = await mlPopulate(MoBaModel.findOne({ _id: req.params.id }),refsList.slice()).lean().exec()
            }
            res.send({
                error: false,
                result_number: moBaModel.length,
                message: moBaModel,
            })
        });

        this.post('/', async (req, res) => {
            console.log(req.body)
            const { error } = MoBaModel.validateOnCreate(req.body)
            if (error) {
                return res.status(400).send({
                    error: true,
                    result_number: 140002,
                    message: 'Input data not correct: ' + error.details[0].message,
                })
            }
        
            var moBaModel = new MoBaModel(_.pick(req.body, changable_props))

            if(isCompanyPrivateData){
                moBaModel.company="5f007ef891e0d33f080b9972"; 
            }

            moBaModel.created_time = Date.now()
            moBaModel.updated_time = Date.now()
        
            const result = await moBaModel.save()
        
            res.send({
                error: false,
                result_number: 1,
                message: result,
            })
        });


        
        this.patch('/:id', async (req, res) => {
            const { error } = MoBaModel.validate(req.body)
            if (error) {
                return res.status(400).send({
                    error: true,
                    result_number: 140002,
                    message: 'Input data not correct: ' + error.details[0].message,
                })
            }


            var moBaModel =null;
            if(isCompanyPrivateData){
                moBaModel = await MoBaModel.findByIdAndUpdate(
                    {
                        _id: req.params.id, 'company': '5f007ef891e0d33f080b9972' 
                    },
                    req.body,
                    { new: true },
                )
            }
            else
            {
                moBaModel = await MoBaModel.findByIdAndUpdate(
                    {
                        _id: req.params.id,
                    },
                    req.body,
                    { new: true },
                )
            }

            moBaModel.updated_time = Date.now()

            const result = await moBaModel.save()
            if (result) {
                res.send({
                    error: false,
                    result_number: 1,
                    message: result,
                })
            } else {
                res.status(404).send({
                    error: true,
                    result_number: 140402,
                    message: 'Record not found',
                })
            }
        });

        this.delete('/:id', async (req, res) => {

            var moBaModel =null;
            if(isCompanyPrivateData){
                moBaModel =await MoBaModel.findByIdAndRemove({
                    _id: req.params.id,'company': '5f007ef891e0d33f080b9972' 
                })
            }
            else
            {
                moBaModel = await MoBaModel.findByIdAndRemove({
                    _id: req.params.id,
                })
            }
        
            if (moBaModel) {
                res.send({
                    error: false,
                    result_number: 1,
                    message: moBaModel,
                })
            } else {
                res.status(404).send({
                    error: true,
                    result_number: 140402,
                    message: 'Record not found',
                })
            }
        });
    }

   /* checkPermissoion=(user_id,)=>{


        return true;
        
    }*/
}


module.exports = RouterManager
